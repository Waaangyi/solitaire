package com.waaangyi;

public class Config {
    public static final int cardHeight = 103;
    public static final int cardWidth = 71;
    //public static final int cardHeight = 206;
    //public static final int cardWidth = 142;
    public static final int horGap = 15;
    public static final int stackGap = 30;
}
