package com.waaangyi;

import com.waaangyi.card.Card;
import com.waaangyi.card.CardT;
import com.waaangyi.card.CardV;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class Game {
    public static List<List<Card>> board = new ArrayList<>();
    public static List<List<Card>> foundation = new ArrayList<>();
    public static Stack<Card> stock = new Stack<>();
    public static Stack<Card> waste = new Stack<>();
    public static void init() {
        //initialize arraylist
        for(int i = 0; i < 7; ++i) {
            board.add(new ArrayList<>());
        }
        for(int i = 0; i < 4; ++i) {
            foundation.add(new ArrayList<>());
        }
        //generate cards
        Stack<Card> cards = new Stack<>();
        CardT[] types = CardT.values();
        CardV[] values = CardV.values();
        for(int i = 0; i < 4; ++i) {
            for(int j = 0; j < 13; ++j) {
                cards.push(new Card(values[j],types[i]));
            }
        }
        Collections.shuffle(cards);
        //tableau
        for(int i = 0; i < 7; ++i) {
            for(int j = 0; j < i + 1; ++j) {
                board.get(i).add(cards.pop());
                board.get(i).get(j).setGrid(j,i);
                board.get(i).get(j).convertCoords();
                board.get(i).get(j).draw();
                board.get(i).get(j).setInTableau(true);
            }
            board.get(i).get(i).setVisible(true);
        }
        //stock
        while(!cards.empty()) {
            Card card = cards.pop();
            card.setVisible(false);
            card.setInTableau(false);
            card.setCoords(Config.horGap, Config.stackGap);
            stock.push(card);
        }
        log("stock size: " + stock.size());

    }
    public static void drawFromStock() {
        if(stock.isEmpty()) {
            return;
        }
        Card card = stock.pop();
        card.setCoords(Config.horGap * 2 + Config.cardWidth, Config.stackGap);
        card.setVisible(true);
        waste.push(card);
    }
    public static void resetStock() {
        if(stock.isEmpty()) {
            while(!waste.isEmpty()) {
                Card card = waste.pop();
                card.setCoords(Config.horGap, Config.stackGap);
                card.setVisible(false);
                stock.push(card);
            }
        }
    }
    public static void moveToStack(int columnID, Card card) {
        if(card.isInTableau()) {
            //move the cards below it too
            List<Card> cards = new ArrayList<>();
            int row = card.getRow();
            int column = card.getColumn();
            for(int i = row; i < board.get(column).size(); ++i) {
                cards.add(Game.board.get(column).get(i));
                //Game.log(drop.getRow() + (i - row + 1) + " " + drop.getColumn());
                cards.get(i - row).setGrid(board.get(columnID).size() + (i - row), columnID);
                cards.get(i - row).convertCoords();
            }
            if (board.get(column).size() > row) {
                board.get(column).subList(row, board.get(column).size()).clear();
            }
            for(Card c: cards) {
                board.get(columnID).add(c);
            }
            if(row - 1 >= 0) {
                board.get(column).get(row - 1).setVisible(true);
            }
        } else {
            //move card
            card.setGrid(board.get(columnID).size(), columnID);
            card.convertCoords();
            card.setInTableau(true);
            //card.resetCoords();
            board.get(columnID).add(card);

        }
    }
    public static void moveToFoundation(int foundationID, Card card) {
        if(card.isInTableau()) {
            board.get(card.getColumn()).remove(board.get(card.getColumn()).size() - 1);
            if(card.getRow() - 1 >= 0) {
                board.get(card.getColumn()).get(card.getRow() - 1).setVisible(true);
            }
        } else {
            waste.pop();
        }
        card.setCoords((Config.horGap + Config.cardWidth) * (3 + foundationID) + Config.horGap, Config.stackGap);
        foundation.get(foundationID).add(card);
    }
    public static boolean canStack(int column, Card card) {
        if(board.get(column).size() == 0)
            return card.value == CardV.KING;
        Card last = board.get(column).get(board.get(column).size() - 1);
        return last.compareTo(card) == 1 && last.diffColor(card);
    }
    public static boolean canFoundation(int foundationColumn, Card card) {
        if(card.isInTableau()) {
            //must be last
            if(card.getRow() != board.get(card.getColumn()).size() - 1) return false;
        }
        for(List<Card> cards: foundation) {
            if(cards.size() != 0 && cards.get(0).type == card.type) {
                //exactly smaller by one
                return cards.get(cards.size() - 1).compareTo(card) == -1;
            }
        }
        //empty must be ace
        return card.value == CardV.ACE;
    }
    public static boolean checkWin() {
        for(List<Card> cards: foundation) {
            if(cards.size() != 13) return false;
        }
        return true;
    }
    public static void log(Object s) {
        System.out.println(Thread.currentThread().getStackTrace()[2] + " " + s.toString());
    }

}
