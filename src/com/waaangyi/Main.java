package com.waaangyi;

import com.waaangyi.card.Card;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) {
        new MainFrame();
    }
}
class MainFrame extends JFrame {
    public MainFrame() {
        super("Solitaire (not soyware created by soydevs!!!)");
        setPreferredSize(new Dimension(700, 900));
        Game.init();
        Board board = new Board();
        getContentPane().add(board);
        pack();
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
class Board extends JPanel {
    private boolean drag = false;
    private Card tableauSelected = null;
    private Card wasteSelected = null;
    private int fromX;
    private int fromY;
    private final Rectangle stockRectangle;
    private final Rectangle wasteRectangle;
    private final List<Rectangle> tableauRectangles;
    private final List<Rectangle> foundationRectangles;
    private boolean winNotification = false;
    public Board() {
        stockRectangle = (Rectangle) Game.stock.get(0).getRectangle().clone();
        wasteRectangle = new Rectangle(stockRectangle.x + Config.cardWidth + Config.horGap, stockRectangle.y,
                Config.cardWidth, Config.cardHeight);
        tableauRectangles = new ArrayList<Rectangle>();
        for(List<Card> cards: Game.board) {
            tableauRectangles.add((Rectangle) cards.get(0).getRectangle().clone());
        }
        foundationRectangles = new ArrayList<Rectangle>();
        for(int i = 0; i < 4; ++i) {
            foundationRectangles.add(new Rectangle((Config.horGap + Config.cardWidth) * (3 + i) + Config.horGap,
                    Config.stackGap,
                    Config.cardWidth, Config.cardHeight));
        }
        //MyMouse moseEvent = new MyMouse();
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                if (e.getButton() == MouseEvent.BUTTON1) {
                    //stock
                    if (stockRectangle.contains(x, y)) {
                        if (Game.stock.isEmpty()) {
                            Game.resetStock();
                        } else {
                            Game.drawFromStock();
                        }
                        repaint();
                        return;
                    }
                    //waste
                    if(wasteRectangle.contains(x,y) && !Game.waste.isEmpty()) {
                        wasteSelected = Game.waste.peek();
                        fromX = x - wasteRectangle.x;
                        fromY = y - wasteRectangle.y;
                        return;
                    }
                    tableauSelected = getBoardClicked(x, y);
                    if (tableauSelected == null || !tableauSelected.getVisible()) {
                        tableauSelected = null;
                        return;
                    }
                    if (tableauSelected != null) {
                        Game.log(tableauSelected.getRow() + " " + tableauSelected.getColumn());
                        fromX = x - tableauSelected.getX();
                        fromY = y - tableauSelected.getY();
                        return;
                    }
                }
                /*
                if(e.getButton() == MouseEvent.BUTTON2) {
                    //God mode, nukes cards
                    Card selected = getBoardClicked(x,y);
                    if(selected != null) {
                        Game.board.get(selected.getColumn()).remove(selected);
                        repaint();
                    }
                }

                 */
            }
            public void mouseReleased(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                if (e.getButton() == MouseEvent.BUTTON1) {
                    fromX = 0;
                    fromY = 0;
                    boolean processed = false;
                    if (drag && tableauSelected != null) {
                        int boardDrop = getBoardDrag(x, y);
                        if (boardDrop != -1 && Game.canStack(boardDrop, tableauSelected)) {
                            //Game.log(boardDrop.getRow() + " " + boardDrop.getColumn());
                            /*
                            boardDrop = Game.board.get(boardDrop.getColumn()).get(Game.board.get(boardDrop.getColumn()).size() - 1);
                            int row = tableauSelected.getRow();
                            int column = tableauSelected.getColumn();
                            List<Card> cards = new ArrayList<>();
                            for (int i = row; i < Game.board.get(column).size(); ++i) {
                                cards.add(Game.board.get(column).get(i));
                                //Game.log(boardDrop.getRow() + (i - row + 1) + " " + boardDrop.getColumn());
                                cards.get(i - row).setGrid(boardDrop.getRow() + (i - row + 1), boardDrop.getColumn());
                                cards.get(i - row).convertCoords();
                            }
                            for (int i = Game.board.get(column).size() - 1; i >= row; --i) {
                                Game.board.get(column).remove(i);
                            }
                            for (Card card : cards) {
                                Game.board.get(boardDrop.getColumn()).add(card);
                            }
                            if (row - 1 >= 0) {
                                Game.board.get(column).get(row - 1).setVisible(true);
                            }

                             */
                            Game.moveToStack(boardDrop, tableauSelected);
                            processed = true;
                        }
                        int foundationDrop = getBoardDragFoundation(x,y);
                        if(foundationDrop != -1 && Game.canFoundation(foundationDrop, tableauSelected)) {
                            Game.moveToFoundation(foundationDrop, tableauSelected);
                            processed = true;
                        }
                        if(!processed) {
                            //reset
                            int row = tableauSelected.getRow();
                            int column = tableauSelected.getColumn();
                            for (int i = row; i < Game.board.get(column).size(); ++i) {
                                Game.board.get(column).get(i).resetCoords();
                            }
                        }
                    }
                    if(drag && wasteSelected != null) {
                        int boardDrop = getBoardDrag(x,y);
                        if(boardDrop != -1 && Game.canStack(boardDrop,wasteSelected)) {
                            //boardDrop = Game.board.get(boardDrop.getColumn()).get(Game.board.get(boardDrop.getColumn()).size() - 1);
                            /*
                            wasteSelected.setGrid(boardDrop.getRow() + 1, boardDrop.getColumn());
                            wasteSelected.convertCoords();
                            wasteSelected.resetCoords();
                            Game.board.get(boardDrop.getColumn()).add(wasteSelected);
                             */
                            Game.moveToStack(boardDrop, wasteSelected);
                            Game.waste.pop();
                            processed = true;
                        }
                        int foundationDrop = getBoardDragFoundation(x,y);
                        if(foundationDrop != -1 && Game.canFoundation(foundationDrop, wasteSelected)) {
                            Game.moveToFoundation(foundationDrop, wasteSelected);
                            processed = true;
                        }
                        if(!processed) {
                            wasteSelected.resetCoords();
                        }

                    }

                    drag = false;
                    tableauSelected = null;
                    wasteSelected = null;
                    repaint();
                    if (Game.checkWin() && !winNotification) {
                        JOptionPane.showMessageDialog(null, "You Won!!!", "Important Info:" , JOptionPane.INFORMATION_MESSAGE);
                        winNotification = true;
                    }
                }
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            //TODO: empty pile detection
            @Override
            public void mouseDragged(MouseEvent e) {
                drag = true;
                int x = e.getX();
                int y = e.getY();
                if(tableauSelected != null) {
                    //dragging cards in the tableau
                    //Graphics graphics = getGraphics();
                    int row = tableauSelected.getRow();
                    int column = tableauSelected.getColumn();
                    int dx = x - (tableauSelected.getRectangle().x + fromX);
                    int dy = y - (tableauSelected.getRectangle().y + fromY);
                    for(int i = row; i < Game.board.get(column).size(); ++i) {
                        Game.board.get(column).get(i).getRectangle().translate(dx,dy);
                    }
                    /*
                    graphics.drawImage(selected.getImage(),
                            selected.getRectangle().x,selected.getRectangle().y,
                            selected.getRectangle().x + Config.cardWidth,selected.getRectangle().y + Config.cardHeight,
                            0,0,
                            selected.getImage().getWidth(), selected.getImage().getHeight(),
                            null);
                     */

                    //in an attempt to prove the soydevs wrong, I have become the soydev
                    repaint();
                    return;
                }
                if(wasteSelected != null) {
                    int dx = x - (wasteSelected.getRectangle().x + fromX);
                    int dy = y - (wasteSelected.getRectangle().y + fromY);
                    wasteSelected.getRectangle().translate(dx,dy);

                    //in an attempt to prove the soydevs wrong, I have become the soydev
                    repaint();
                    //Game.log(wasteSelected.getRectangle().x + " " + wasteSelected.getRectangle().y);
                }
            }
        });
    }
    private Card getBoardClicked(int x, int y) {
        for(List<Card> list: Game.board) {
            for(int i = list.size() - 1; i >= 0; --i) {
                if(list.get(i).getRectangle().contains(x,y)) return list.get(i);
            }
        }
        return null;
    }
    private int getBoardDrag(int x, int y) {
        for(int i = 0; i < tableauRectangles.size(); ++i) {
            if(tableauRectangles.get(i).contains(x,y)) {
                return i;
            }
        }
        if(tableauSelected != null) {
            for(List<Card> list: Game.board) {
                for(int i = list.size() - 1; i >= 0; --i) {
                    if(list.get(i).getRectangle().contains(x,y) && list.get(i) != tableauSelected) return list.get(i).getColumn();
                }
            }
        }
        if(wasteSelected != null) {
            for(List<Card> list: Game.board) {
                for(int i = list.size() - 1; i >= 0; --i) {
                    if(list.get(i).getRectangle().contains(x,y) && list.get(i) != wasteSelected) return list.get(i).getColumn();
                }
            }
        }
        return -1;
    }
    private int getBoardDragFoundation(int x, int y) {
        for(int i = 0; i < foundationRectangles.size(); ++i) {
            if(foundationRectangles.get(i).contains(x,y)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        super.paintComponent(g);
        //Stroke stroke = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
        g2.setColor(Color.black);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        //g2.setStroke(stroke);
        //TODO: render the stock, waste, and foundations

        //blank rectangles
        g2.draw(wasteRectangle);
        g2.draw(stockRectangle);
        for(Rectangle rectangle: tableauRectangles) {
            g2.draw(rectangle);
        }
        for(Rectangle rectangle: foundationRectangles) {
            g2.draw(rectangle);
        }
        //stock
        if(!Game.stock.isEmpty()) {
            drawCard(Game.stock.peek(), g2);
        }

        //foundation top
        for(List<Card> cards: Game.foundation) {
            if(cards.size() != 0) {
                drawCard(cards.get(cards.size() - 1), g2);
            }
        }
        //tableau cards
        for(List<Card> list: Game.board) {
            for(Card card: list) {
                drawCard(card, g2);
                /*
                g2.drawImage(card.getImage(),
                    card.getRectangle().x,card.getRectangle().y,
                    card.getRectangle().x + Config.cardWidth,card.getRectangle().y + Config.cardHeight,
                    0,0,
                    card.getImage().getWidth(), card.getImage().getHeight(),
                    null);
                    */
                g2.draw(card.getRectangle());
            }
        }
        if(tableauSelected != null) {
            //paint the selected last
            int row = tableauSelected.getRow();
            int column = tableauSelected.getColumn();
            for(int i = row; i < Game.board.get(column).size(); ++i) {
                //Game.board.get(column).get(row).getRectangle().translate(dx,dy);
                drawCard(Game.board.get(column).get(i), g2);
                /*
                g2.drawImage(Game.board.get(column).get(i).getImage(),
                        Game.board.get(column).get(i).getRectangle().x,Game.board.get(column).get(i).getRectangle().y,
                        Game.board.get(column).get(i).getRectangle().x + Config.cardWidth,Game.board.get(column).get(i).getRectangle().y + Config.cardHeight,
                        0,0,
                        Game.board.get(column).get(i).getImage().getWidth(), Game.board.get(column).get(i).getImage().getHeight(),
                        null);

                 */
                g2.draw(Game.board.get(column).get(i).getRectangle());
            }
        }
        //waste
        if(Game.waste.size()>=2) {
            drawCard(Game.waste.get(Game.waste.size() - 2), g2);
            drawCard(Game.waste.get(Game.waste.size() - 1), g2);
        }
        if(Game.waste.size() == 1) {
            drawCard(Game.waste.get(Game.waste.size() - 1), g2);
        }
    }
    private void drawCard(Card card, Graphics2D g2) {
        g2.drawImage(card.getImage(),
                card.getRectangle().x,card.getRectangle().y,
                card.getRectangle().x + Config.cardWidth,card.getRectangle().y + Config.cardHeight,
                0,0,
                card.getImage().getWidth(), card.getImage().getHeight(),
                null);
    }
}
