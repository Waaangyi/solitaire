package com.waaangyi.card;

public enum CardV {
    ACE("ace"),
    TWO("2"),
    THREE("3"),
    FOUR("4"),
    FIVE("5"),
    SIX("6"),
    SEVEN("7"),
    EIGHT("8"),
    NINE("9"),
    TEN("10"),
    JACK("jack"),
    QUEEN("queen"),
    KING("king");
    private final String key;
    CardV(String key) {
        this.key = key;
    }
    @Override
    public String toString() {
        return this.key;
    }

    public String getKey() {
        return key;
    }

    public boolean isSmallerThan(CardV card) {
        return this.compareTo(card) < 0;
    }
    public boolean isGreaterThan(CardV card) {
        return this.compareTo(card) > 0;
    }
}
