package com.waaangyi.card;

import com.waaangyi.Config;
import com.waaangyi.Game;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Card implements Comparable{
    public final CardV value;
    public final CardT type;
    private boolean inTableau;
    private boolean show;
    private int row, column;
    private int x,y;
    private Rectangle rectangle = new Rectangle();
    private BufferedImage image;
    private BufferedImage blankImage;
    // stock, waste, foundations
    public Card(CardV value, CardT type) {
        this.value = value;
        this.type = type;
        this.show = false;
        try {
            image = ImageIO.read(new File(String.format("assets/%s_of_%s.png", this.value.getKey(), this.type.getKey())));
            blankImage = ImageIO.read(new File("assets/back.jpg"));
        } catch (IOException e) {
            Game.log(String.format("assets/%s_of_%s.png", this.value.getKey(), this.type.getKey()));
            //e.printStackTrace();
        }
    }

    public boolean isInTableau() {
        return inTableau;
    }

    public void setInTableau(boolean inTableau) {
        this.inTableau = inTableau;
    }

    public BufferedImage getImage() {
        if(show)
            return this.image;
        return this.blankImage;
    }
    public void setVisible(boolean show) {
        this.show = show;
    }
    public boolean getVisible() {
        return this.show;
    }
    public void setGrid(int row, int column) {
        this.row = row;
        this.column = column;
    }
    public void convertCoords() {
        setCoords(Config.horGap+(Config.horGap + Config.cardWidth) * column,
                (Config.stackGap) * row + Config.stackGap + Config.cardHeight * 2);
    }

    public void setCoords(int x,int y) {
        this.x = x;
        this.y = y;
        //draw the rectangle too
        draw();
        resetCoords();
    }

    public void resetCoords() {
        this.rectangle.setLocation(this.x,this.y);
    }

    public void draw() {
        rectangle = new Rectangle(this.x,this.y, Config.cardWidth, Config.cardHeight);
    }

    public Rectangle getRectangle() {
        return this.rectangle;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public int compareTo(Object o) {
        return this.value.compareTo(((Card)o).value);
    }

    public boolean diffColor(Card card) {
        if(this.type.equals(card.type)) return false;
        if(this.type.ordinal() < 2 && card.type.ordinal() >= 2) {
            return true;
        }
        return this.type.ordinal() >= 2 && card.type.ordinal() < 2;
    }
}
