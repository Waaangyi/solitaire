package com.waaangyi.card;

public enum CardT {
    CLUBS("clubs"),
    SPADES("spades"),
    DIAMONDS("diamonds"),
    HEARTS("hearts");
    private final String key;
    CardT(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
